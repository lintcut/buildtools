#include "stdafx.h"


BOOL WINAPI DllMain(_In_ HINSTANCE hDLL, _In_ DWORD dwReason, _In_ LPVOID lpvReserved)
{
    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        ::DisableThreadLibraryCalls(hDLL);
        break;
    case DLL_PROCESS_DETACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
            break;
    }

    return TRUE;
}

void WINAPI SayHello()
{
    std::cout << "Hello, World!" << std::endl;
}