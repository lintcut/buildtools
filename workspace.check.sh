#!/bin/sh

# 1. Check Global Variables
echo ""
echo "Step 1: Checking global variables ..."
#    Var: 'WORKSPACE'
if [ "$WORKSPACE" == "" ]; then
    echo "  Failed: 'WORKSPACE' is not defined"
    exit 1
else
    echo "  Passed: WORKSPACE=$WORKSPACE"
fi
#    Var: 'OSNAME'
if [ "$OSNAME" == "" ]; then
    echo "  Failed: 'OSNAME' is not defined"
    exit 1
else
    echo "  Passed: OSNAME=$OSNAME"
fi
#    Var: '_NT_SYMBOL_PATH'
if [ "$OSNAME" == "Win" ]; then
    if [ "$_NT_SYMBOL_PATH" == "" ]; then
        echo "  Warning: '_NT_SYMBOL_PATH' is not set"
    else
        echo "  Passed: _NT_SYMBOL_PATH=$_NT_SYMBOL_PATH"
    fi
fi

# 2. Check directories
echo "Step 2: Checking directories ..."
#    Dir: '$WORKSPACE'
if [ ! -d $WORKSPACE ]; then
    echo "  Failed: Directory 'WORKSPACE' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE' exists"
fi
#   Dir: '$WORKSPACE/buildtools'
if [ ! -d $WORKSPACE/buildtools ]; then
    echo "  Failed: Directory 'WORKSPACE/buildtools' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/buildtools' exists"
fi
#       Dir: '$WORKSPACE/buildtools/makefiles'
if [ ! -d $WORKSPACE/buildtools/makefiles ]; then
    echo "  Failed: Directory 'WORKSPACE/buildtools/makefiles' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/buildtools/makefiles' exists"
fi
#       Dir: '$WORKSPACE/buildtools/templates'
if [ ! -d $WORKSPACE/buildtools/templates ]; then
    echo "  Failed: Directory 'WORKSPACE/buildtools/templates' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/buildtools/templates' exists"
fi
#    Dir: '$WORKSPACE/src'
if [ ! -d $WORKSPACE/src ]; then
    echo "  Failed: Directory 'WORKSPACE/src' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/src' exists"
fi
#    Dir: '$WORKSPACE/pkgs'
if [ ! -d $WORKSPACE/pkgs ]; then
    echo "  Failed: Directory 'WORKSPACE/pkgs' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/pkgs' exists"
fi
#    Dir: '$WORKSPACE/src/external'
if [ ! -d $WORKSPACE/src/external ]; then
    echo "  Failed: Directory 'WORKSPACE/src/external' doesn't exist"
    exit 1
else
    echo "  Passed: Directory 'WORKSPACE/src/external' exists"
fi
#    Dir: '$WORKSPACE/symbols'
if [ ! -d $WORKSPACE/symbols ]; then
    echo "  Warning: Directory 'WORKSPACE/symbols' doesn't exist"
else
    echo "  Passed: Directory 'WORKSPACE/symbols' exists"
fi
#    Dir: '$WORKSPACE/symbols/windows'
if [ ! -d $WORKSPACE/symbols/windows ]; then
    echo "  Warning: Directory 'WORKSPACE/symbols/windows' doesn't exist"
else
    echo "  Passed: Directory 'WORKSPACE/symbols/windows' exists"
fi
#    Dir: '$WORKSPACE/symbols/private'
if [ ! -d $WORKSPACE/symbols/private ]; then
    echo "  Warning: Directory 'WORKSPACE/symbols/private' doesn't exist"
else
    echo "  Passed: Directory 'WORKSPACE/symbols/private' exists"
fi
#    Dir: '$WORKSPACE/debug'
if [ ! -d $WORKSPACE/debug ]; then
    echo "  Warning: Directory 'WORKSPACE/debug' doesn't exist"
else
    echo "  Passed: Directory 'WORKSPACE/debug' exists"
fi

# 3. Check tools
echo "Step 3: Checking tools ..."
if [ "$OSNAME" == "Win" ]; then
    export ProgramDir86='C:/Program Files (x86)'
    test -d "$ProgramDir86" || export ProgramDir86='C:/Program Files'
    # Check Visual Studio
    echo "  [Visual Studio]"
    test -d "$ProgramDir86/Microsoft Visual Studio 15.0/VC/bin" && echo "    - Visual Studio 2017 (15.0)"
    test -d "$ProgramDir86/Microsoft Visual Studio 14.0/VC/bin" && echo "    - Visual Studio 2015 (14.0)"
    test -d "$ProgramDir86/Microsoft Visual Studio 12.0/VC/bin" && echo "    - Visual Studio 2013 (12.0)"
    # Check Windows Kits
    echo "  [Windows Kits]"
    if [ -d "$ProgramDir86/Windows Kits/10/Include/10.0.15063.0" ]; then
        echo "    - 10.0.15063"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.15063.0/um" && echo "      * SDK"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.15063.0/km" && echo "      * WDK"
    fi
    if [ -d "$ProgramDir86/Windows Kits/10/Include/10.0.14393.0" ]; then
        echo "    - 10.0.14393"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.14393.0/um" && echo "      * SDK"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.14393.0/km" && echo "      * WDK"
    fi
    if [ -d "$ProgramDir86/Windows Kits/10/Include/10.0.10240.0" ]; then
        echo "    - 10.0.10240"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.10240.0/um" && echo "      * SDK"
        test -d "$ProgramDir86/Windows Kits/10/Include/10.0.10240.0/km" && echo "      * WDK"
    fi
    if [ -d "$ProgramDir86/Windows Kits/8.1/Include" ]; then
        echo "    - 8.1"
        test -d "$ProgramDir86/Windows Kits/8.1/Include/um" && echo "      * SDK"
        test -d "$ProgramDir86/Windows Kits/8.1/Include/km" && echo "      * WDK"
    fi
    echo "  [Git]"
    test git && echo "    $(git --version)" || echo "    Not installed"
    echo "  [Make]"
    test make && echo "    $(make -v | grep 'GNU Make')" || echo "    Not installed"
elif [ "$OSNAME" == "Mac" ]; then
    echo "Done"
elif [ "$OSNAME" == "Linux" ]; then
    echo "Done"
else
    # Bad OS
    echo "Bad OS type ($OSNAME)"
    exit 1
fi

# 4. Check external library
echo "Step 4: Checking external library ..."
echo "  [boost]"
test -d "$WORKSPACE/src/external/boost" && echo "    - Installed" || "    - Not found"
echo "  [openssl]"
test -d "$WORKSPACE/src/external/openssl" && echo "    - Installed" || "    - Not found"
echo "  [zlib]"
test -d "$WORKSPACE/src/external/zlib" && echo "    - Installed" || "    - Not found"
echo "  [google/cpplint]"
test -d "$WORKSPACE/src/external/google/styleguide/cpplint" && echo "    - Installed" || "    - Not found"
echo "  [google/googletest]"
test -d "$WORKSPACE/src/external/google/googletest" && echo "    - Installed" || "    - Not found"
echo "  [google/glog]"
test -d "$WORKSPACE/src/external/google/glog" && echo "    - Installed" || "    - Not found"
echo "  [google/crc32c]"
test -d "$WORKSPACE/src/external/google/crc32c" && echo "    - Installed" || "    - Not found"

echo ''
echo 'Workspace is in good condition'
exit 0