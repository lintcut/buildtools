#!/bin/sh

if [ "$1" == "" ]; then
    echo "Usage:"
    echo "  ./workspace.create.sh <path>"
    exit 1
fi

TargetWorkspaceDir=$1

if [ -d "$TargetWorkspaceDir" ]; then
    echo "Workspace already exist"
    exit 1
fi

#
# Create directories
#

echo "Creating folders ..."

test -e $(mkdir "$TargetWorkspaceDir") && echo "  - $TargetWorkspaceDir" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/src") && echo "  - $TargetWorkspaceDir/src" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/src/external") && echo "  - $TargetWorkspaceDir/src/external" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/src/bitbucket") && echo "  - $TargetWorkspaceDir/src/bitbucket" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/src/github") && echo "  - $TargetWorkspaceDir/src/github" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/pkgs") && echo "  - $TargetWorkspaceDir/src" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/docs") && echo "  - $TargetWorkspaceDir/docs" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/symbols") && echo "  - $TargetWorkspaceDir/symbols" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/symbols/private") && echo "  - $TargetWorkspaceDir/symbols/private" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/symbols/windows") && echo "  - $TargetWorkspaceDir/symbols/windows" || exit 1
test -e $(mkdir "$TargetWorkspaceDir/temp") && echo "  - $TargetWorkspaceDir/temp" || exit 1

#
# Get external libraries
#

#  -> My buildtools
cd "$TargetWorkspaceDir"
git clone git@bitbucket.org:lintcut/buildtools.git ./buildtools

#  -> boost
cd "$TargetWorkspaceDir/src/external"
git clone --recursive git@github.com:boostorg/boost.git ./boost
cd boost
git checkout boost-1.65.1
if [ "$OSNAME" == "Win" ]; then
    ./bootstrap.bat
else
    ./bootstrap.sh
fi
./b2 headers

#  -> openssl
cd "$TargetWorkspaceDir/src/external"
git clone git@github.com:openssl/openssl.git ./openssl
cd openssl
git checkout OpenSSL_1_1_0f

#  -> zlib
cd "$TargetWorkspaceDir/src/external"
git clone git@github.com:madler/zlib.git ./zlib
cd zlib
git checkout v1.2.11

#  Google
mkdir -p "$TargetWorkspaceDir/src/external/google"
#  -> coding style
git clone git@github:google/styleguide.git ./styleguide
#  -> googletest
git clone git@github.com:google/googletest.git ./googletest
#  -> glog
git clone git@github.com:google/glog.git ./glog
#  -> crc32c
git clone git@github.com:google/crc32c.git ./crc32c

